#ifndef _KNOBS_H_
#define _KNOBS_H_

#include <stdint.h>
#include <stdbool.h>
#include "mzapo_regs.h"

/* -------------------------------------- GLOBAL VARIABLES ------------------------------------------ */

// current value of all knobs as unsigned int
uint32_t *knobs;
// current value of each knob's spinning and knob's pressing
uint8_t *knob_click;
uint8_t *knob_red;
uint8_t *knob_green;
uint8_t *knob_blue;

/* -------------------------------------- FUNCTION DECLARATAION ------------------------------------------ */

/*
 * Priradi knob promennym hardwarovou adresu.
 * Assign hardware address to knobs' variables.
 */
void init_knobs();

#endif // _KNOBS_H_
