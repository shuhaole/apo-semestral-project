#include <math.h>
#include <assert.h>
#include <stdint.h>

/**
 * SOURCE: www.programmingalgorithms.com
 */

typedef struct RGB
{
	unsigned char R;
	unsigned char G;
	unsigned char B;
}RGB888;

typedef struct HSV
{
	double H;
	double S;
	double V;
}HSV;

static double Min(double a, double b) {
	return a <= b ? a : b;
}

static double Max(double a, double b) {
	return a >= b ? a : b;
}

/**
 * Computes the hsv value of color in rgb
 */
HSV RGBToHSV(RGB888 rgb) {
	double delta, min;
	double h = 0, s, v;

	min = Min(Min(rgb.R, rgb.G), rgb.B);
	v = Max(Max(rgb.R, rgb.G), rgb.B);
	delta = v - min;

	if (v == 0.0)
		s = 0;
	else
		s = delta / v;

	if (s == 0)
		h = 0.0;

	else {
		if (rgb.R == v)
			h = (rgb.G - rgb.B) / delta;
		else if (rgb.G == v)
			h = 2 + (rgb.B - rgb.R) / delta;
		else if (rgb.B == v)
			h = 4 + (rgb.R - rgb.G) / delta;

		h *= 60;

		if (h < 0.0)
			h = h + 360;
	}

	HSV hsv;
	hsv.H = h;
	hsv.S = s;
	hsv.V = v / 255;

	return hsv;
}

/**
 * Computes the rgb value of color in hsv
 */
RGB888 HSVToRGB(HSV hsv) {
	double r = 0, g = 0, b = 0;

	if (hsv.S == 0)
	{
		r = hsv.V;
		g = hsv.V;
		b = hsv.V;
	}
	else
	{
		int i;
		double f, p, q, t;

		if (hsv.H == 360)
			hsv.H = 0;
		else
			hsv.H = hsv.H / 60;

		i = (int)trunc(hsv.H);
		f = hsv.H - i;

		p = hsv.V * (1.0 - hsv.S);
		q = hsv.V * (1.0 - (hsv.S * f));
		t = hsv.V * (1.0 - (hsv.S * (1.0 - f)));

		switch (i)
		{
		case 0:
			r = hsv.V;
			g = t;
			b = p;
			break;

		case 1:
			r = q;
			g = hsv.V;
			b = p;
			break;

		case 2:
			r = p;
			g = hsv.V;
			b = t;
			break;

		case 3:
			r = p;
			g = q;
			b = hsv.V;
			break;

		case 4:
			r = t;
			g = p;
			b = hsv.V;
			break;

		default:
			r = hsv.V;
			g = p;
			b = q;
			break;
		}

	}

	RGB888 rgb;
	rgb.R = r * 255;
	rgb.G = g * 255;
	rgb.B = b * 255;

	return rgb;
}

uint16_t rgb888torgb565m(RGB888 rgb888Pixel) {
    uint8_t red   = rgb888Pixel.R;
    uint8_t green = rgb888Pixel.G;
    uint8_t blue  = rgb888Pixel.B;

    uint16_t b = (blue >> 3) & 0x1f;
    uint16_t g = ((green >> 2) & 0x3f) << 5;
    uint16_t r = ((red >> 3) & 0x1f) << 11;

    return (uint16_t) (r | g | b);
}

/**
 * Recreate RGB888 from struct to one hex number  
 */
unsigned int recreateRGB(RGB888 color) {   
    return ((color.R & 0xff) << 16) + ((color.G & 0xff) << 8) + (color.B & 0xff);
}

uint16_t our888to565(unsigned int color) {
	uint8_t *bytes = &color;
	RGB888 rgb;
	rgb.R = bytes[1];
	rgb.G = bytes[2];
	rgb.B = bytes[3];

	return rgb888torgb565m(rgb);
}

unsigned int our565to888(uint16_t color) {

	uint8_t r = ((color >> 11) & 0x1F);
	uint8_t g = ((color >> 5) & 0x3F);
	uint8_t b = (color & 0x1F);

	r = ((((color >> 11) & 0x1F) * 527) + 23) >> 6;
	g = ((((color >> 5) & 0x3F) * 259) + 33) >> 6;
	b = (((color & 0x1F) * 527) + 23) >> 6;

	unsigned int ret = r << 16 | g << 8 | b;
	return ret;
}

/**
 * Create RGB888 from HSV color
 */
unsigned long createRGBfromHSV(HSV color) {   
    return recreateRGB(HSVToRGB(color));
}

uint16_t hsvtorgb565old(HSV hsv){
	return rgb888torgb565m(HSVToRGB(hsv));	
}

uint16_t hsvtorgb565(double h, double s, double v){
	HSV hsv;
	hsv.H = h;
	hsv.S = s;
	hsv.V = v;
	return rgb888torgb565m(HSVToRGB(hsv));	
}

unsigned int hsvtorgb888(double h, double s, double v){

	HSV hsv;
	hsv.H = h;
	hsv.S = s;
	hsv.V = v;

	RGB888 rgb = HSVToRGB(hsv);

    return recreateRGB(rgb);
}
