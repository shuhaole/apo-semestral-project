#include "menu_network.h"

void menu_network() {
    Found_listeners *found_listeners = (Found_listeners*)malloc(sizeof(Found_listeners));
	if (found_listeners == NULL) {
		printf("ERROR alokace\n");
		return;
	}
    found_listeners->count = 0;
    found_listeners->allocated_size = 10;
    found_listeners->listeners_addr = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in) * 10);
	if (found_listeners->listeners_addr == NULL) {
		printf("ERROR alokace\n");
		free(found_listeners);
		return;
	}
    uint8_t last_valid_green_value = *knob_green / 4;
    uint8_t valid_green_value = *knob_green / 4;
    int green_change = 0;
    bool repaint = false;
    int shown_address = 0;
    show_menu_network(*found_listeners, shown_address);

	Master_listen_args *args = (Master_listen_args*)malloc(sizeof(Master_listen_args));
	args->found_listeners = found_listeners;
	args->repaint = &repaint;
    pthread_t thread_id_master_listen; 
    pthread_create(&thread_id_master_listen, NULL, master_listen, (void*)args); 

    send_broadcast();
    while (true) {
    	repaint = false;
        // pressing red knob - found IPs reset and sending broadcast
        if (*knob_click == 4) {
			while (*knob_click == 4) {
				printf(".");
			}
			// resets found IPs
			if (found_listeners->listeners_addr != NULL) {
				free(found_listeners->listeners_addr);
			}
    		found_listeners->count = 0;
    		found_listeners->allocated_size = 10;
			found_listeners->listeners_addr = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in) * 10);	
			if (found_listeners->listeners_addr == NULL) {
				printf("ERROR alokace\n");
				free(found_listeners);
				return;
			}
			// sends broadcast
            send_broadcast();
        }
        // pressing green knob - sending instructions to chosen IP
        else if (*knob_click == 2) {
			while (*knob_click == 2) {
				printf(".");
			}
            if (found_listeners->count > 0) {
                send_control(found_listeners->listeners_addr[shown_address]);
            }
        }
		// pressing blue knob - back
        else if (*knob_click == 1) {
			while (*knob_click == 1) {
				printf(".");
			}
            break;
        }
        valid_green_value = *knob_green / 4;
		// computes current knobs' spin change value
        green_change = 0;
		if (last_valid_green_value != valid_green_value) {
			// spin of the blue knob in the positive way
            if ((valid_green_value > last_valid_green_value) || (last_valid_green_value >= 62 && valid_green_value < 32)) {
                green_change = 1;
            }
			// spin of the blue knob in the negative way
            else if ((valid_green_value < last_valid_green_value) || (last_valid_green_value <= 1 && valid_green_value > 32)) {
                green_change = -1;
            }
            last_valid_green_value = valid_green_value;
			repaint = true;
		}
        shown_address += green_change;
        if (shown_address < 0) {
            shown_address = found_listeners->count - 1;
        }
        else if (shown_address == found_listeners->count) {
            shown_address = 0;
        }
		if (repaint) {
			printf("repaint\n");
    		show_menu_network(*found_listeners, shown_address);
		}
    }
	pthread_kill(thread_id_master_listen, 0);
	// frees allocated memory
	if (found_listeners->listeners_addr != NULL) {
		free(found_listeners->listeners_addr);
	}
	if (found_listeners != NULL) {
		free(found_listeners);
	}
}

void show_menu_network(Found_listeners found_listeners, int shown_address) {
    set_background_color(WHITE);
    // upper row
    char *txt = english ? "Found IPs:" : "Nalezene IP:";
    Point point = find_string_start(txt, 2, 0, 0, LCD_HEIGHT_FIFTH, LCD_WIDTH);
    str2frame(txt, point.y, point.x, BLACK, 2);
    // if nothing was found
    if (found_listeners.count == 0) {   
        txt = english ? "No IP found" : "IP nenalezena";
        point = find_string_start(txt, font_size, LCD_HEIGHT_FIFTH, 0, LCD_HEIGHT_FOUR_FIFTHS, LCD_WIDTH);
        str2frame(txt, point.y, point.x, BLACK, font_size);
    }
    // if any IP was found
    else {  
		printf("shown_address %d\n", shown_address);
        // number of shown IP from all found
		char a_txt[] = {(shown_address + 1) / 10 + 48, ((shown_address + 1) % 10) + 48, '/', (found_listeners.count / 10) + 48, (found_listeners.count % 10) + 48, ':', 0};
		txt = a_txt;
        point = find_string_start(txt, font_size, LCD_HEIGHT_FIFTH, 0, LCD_HEIGHT_THREE_FIFTHS, LCD_WIDTH_QUARTER);
        str2frame(txt, point.y, point.x, BLACK, font_size);
        // IP address
		printf("IP: %u\n", found_listeners.listeners_addr[shown_address].sin_addr.s_addr);
		print_str_ip(found_listeners.listeners_addr[shown_address].sin_addr.s_addr);
        txt = english ? "Control" : "Ovladat";
        point = find_string_start(txt, font_size, LCD_HEIGHT - MASK_ROWS * font_size, LCD_WIDTH_THIRD, MASK_ROWS * font_size, LCD_WIDTH_THIRD);
        str2frame(txt, point.y, point.x, BLACK, font_size);
    }
    // knobs' functions description
    txt = english ? "Refresh" : "Aktualizovat";
    point = find_string_start(txt, font_size, LCD_HEIGHT - MASK_ROWS * font_size, 0, MASK_ROWS * font_size, LCD_WIDTH_THIRD);
    str2frame(txt, point.y, point.x, BLACK, font_size);
    txt = english ? "Back" : "Zpet";
    point = find_string_start(txt, font_size, LCD_HEIGHT - MASK_ROWS * font_size, LCD_WIDTH_TWO_THIRDS, MASK_ROWS * font_size, LCD_WIDTH_THIRD);
    str2frame(txt, point.y, point.x, BLACK, font_size);
    // repaints display
    repaint_lcd();
}

void print_str_ip(unsigned int ui_ip) {
	char *byte = (char *)&ui_ip;
	char byte0 = byte[0];
	char byte1 = byte[1];
	char byte2 = byte[2];
	char byte3 = byte[3];
	char str_ip[] = {byte0 / 100 + 48, (byte0 % 100) / 10 + 48, byte0 % 10 + 48, '.', byte1 / 100 + 48, (byte1 % 100) / 10 + 48, byte1 % 10 + 48, '.',\
					 byte2 / 100 + 48, (byte2 % 100) / 10 + 48, byte2 % 10 + 48, '.', byte3 / 100 + 48, (byte3 % 100) / 10 + 48, byte3 % 10 + 48, 0};
	char *ret_str = str_ip;
	Point point = find_string_start(ret_str, font_size, LCD_HEIGHT_FIFTH, LCD_WIDTH_QUARTER, LCD_HEIGHT_THREE_FIFTHS, LCD_WIDTH_THREE_QUARTERS);
    str2frame(ret_str, point.y, point.x, BLACK, font_size);
}

