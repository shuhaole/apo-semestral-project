#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include "str_utils.h"
#include "rgb565_colors.h"
#include <time.h>

/* -------------------------------------- FUNCTION DECLARATAION ------------------------------------------ */

/*
 * Zobrazi uvodni animaci.
 * Shows start screen's animation.
 */
void start_screen();
   
/*
 * Zobrazi ukoncovaci animaci v anglicitine (pokud je argument true) nebo cestine (pokud je argument false).
 * Shows quit screen's animation in english (if the arguments is true) or czech (if the arguments is false).
 */ 
void quit_screen(bool english);

#endif // _ANIMATION_H_
