#include "lcd.h"

void init_lcd() {
	lcd = map_phys_address(PARLCD_REG_BASE_PHYS, 4, false);
}

void repaint_lcd() {
	parlcd_write_cmd(lcd, 0x2c);	// sets display's pointer to the screen's start possition
	for (int y = 0; y < LCD_HEIGHT; ++y) {
		for (int x = 0; x < LCD_WIDTH; ++x) {
			parlcd_write_data(lcd, pixels[y][x]);
		}
	}
}

void set_background_color(uint16_t color) {
	set_rect_color(color, 0, 0, LCD_HEIGHT, LCD_WIDTH);
}

void set_rect_color(uint16_t color, int y_start, int x_start, int y_stop, int x_stop) {
    for (int y = y_start; y < y_stop; ++y) {
		for (int x = x_start; x < x_stop; ++x) {
			pixels[y][x] = color;
		}
	}
}
