#ifndef _DIODES_H_
#define _DIODES_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>
#include "mem_base.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

/* -------------------------------------- DEFINED STRUCTURES ------------------------------------------ */

/*
 * Popis barvy v RGB565 a hodnotach hue, saturation, value HSV modelu.
 * Color's description in RGB565 and HSV model's values.
 */
typedef struct {
	uint16_t rgb565;
	double hue;
	double saturation;
	double value;
} Color;

/*
 * Trojice bool priznaku zvlast pro levou, pravou a obe diody.
 * Three bool indexes for left, right and both diodes.
 */
typedef struct {
	bool left;
	bool right;
	bool both;
} Diodes_bool;

/*
 * Informace o nastaveni blikani urcite diody.
 * Information about diode's current flashing set up.
 */
typedef struct {
	bool flashing_on;
	int flashing_time;
	int flashing_gab;
	int phase_shift;
} Flashing;

/* -------------------------------------- GLOBAL VARIABLES ------------------------------------------ */

// diodes' colors
Color left_diode;           // color which the diode has at the moment
Color left_diode_changed;   // color which the user is choosing at the moment
Color right_diode;          // color which the diode has at the moment
Color right_diode_changed;  // color which the user is choosing at the moment
// sliding change of diodes
volatile Diodes_bool sliding;
// diodes' flashing
volatile Flashing flashing_left;
volatile Flashing flashing_right;
volatile Flashing flashing_both;
// threads' ID
pthread_t thread_id_left_diode;
pthread_t thread_id_right_diode;
pthread_t thread_id_left_slide;
pthread_t thread_id_right_slide;    
pthread_t thread_id_both_slide; 
// threads' mutexes
pthread_mutex_t left_diode_lock;
pthread_mutex_t left_diode_changed_lock;
pthread_mutex_t right_diode_lock;
pthread_mutex_t right_diode_changed_lock;
pthread_mutex_t sliding_lock;
pthread_mutex_t flashing_left_lock;
pthread_mutex_t flashing_right_lock;
pthread_mutex_t flashing_both_lock;

/* -------------------------------------- FUNCTION DECLARATAION ------------------------------------------ */

/*
 * Inicializuje prvotni nastaveni barev diod a spusti ovladaci vlakna.
 * Initialize original values of diodes and starts controling threads.
 */
void init_diodes();

/*
 * Ukonci ovladaci vlakna diod a mutex zamky.
 * Kills diodes' controling threads and destroys mutexes.
 */
void kill_diodes();

/*
 * Funkce volana z vlakna ovladajiciho levou diodu, ktera periodicky aktualizuje barvu a zhasina diodu podle nastaveni blikani.
 * Function called from the left diode's control thread, which periodically updates the color and switchs the diode on/off according to set flashing.
 */
void *left_diode_light();

/*
 * Funkce volana z vlakna ovladajiciho pravou diodu, ktera periodicky aktualizuje barvu a zhasina diodu podle nastaveni blikani.
 * Function called from the right diode's control thread, which periodically updates the color and switchs the diode on/off according to set flashing.
 */
void *right_diode_light();

/*
 * Funkce volana z vlakna ovladajiciho postupnou zmenu barvy leve diody mezi aktualni a zvolenou barvou.
 * Function called from the left diode's sliding control thread, which changes the color between two colors.
 */
void *left_diode_slide();

/*
 * Funkce volana z vlakna ovladajiciho postupnou zmenu barvy prave diody mezi aktualni a zvolenou barvou.
 * Function called from the right diode's sliding control thread, which changes the color between two colors.
 */
void *right_diode_slide();

/*
 * Funkce volana z vlakna ovladajiciho postupnou zmenu barvy obou diody mezi aktualni a zvolenou barvou.
 * Function called from the both diodes' sliding control thread, which changes the color between two colors.
 */
void *both_diodes_slide();

/*
 * Vypocita barvu leve diody z globalnich promennych reprezentujici HSV barvu a ulozi ji do globalni promenne reprezentujici RGB565 barvu.
 * Computes the color of the left diode from HSV variables and saves it in the RGB565 variable in the global structure.
 */
void compute_left_diode_color();

/*
 * Vypocita barvu prave diody z globalnich promennych reprezentujici HSV barvu a ulozi ji do globalni promenne reprezentujici RGB565 barvu.
 * Computes the color of the right diode from HSV variables and saves it in the RGB565 variable in the global structure.
 */
void compute_right_diode_color();

/*
 * Ulozi left_diode_changed.rgb565 do left_diode.rgb565. Prepocita left_diode.rgb565 na RGB888 hodnotu a tu zobrazi v leve diode.
 * Saves left_diode_changed.rgb565 into left_diode.rgb565. Converts left_diode.rgb565 to RGB888 value and shows it in the left diode.
 */
void set_left_diode();

/*
 * Ulozi right_diode_changed.rgb565 do right_diode.rgb565. Prepocita right_diode.rgb565 na RGB888 hodnotu a tu zobrazi v prave diode.
 * Saves right_diode_changed.rgb565 into right_diode.rgb565. Converts right_diode.rgb565 to RGB888 value and shows it in the right diode.
 */
void set_right_diode();

/*
 * Zkopiruje left_diode_changed do right_diode_changed.
 * Copies left_diode_changed to right_diode_changed.
 */
void copy_from_left_diode_to_right();

/*
 * Zkopiruje right_diode_changed do left_diode_changed.
 * Copies right_diode_changed to left_diode_changed.
 */
void copy_from_right_diode_to_left();

/*
* Prohodi barvy diod.
* Switches diodes' colors.
*/
void switch_diodes();

/*
 * Zmeni HSV hodnoty barvy leve diody a prepocita jeji left_diode_changed.rgb565 reprezentaci.
 * Changes left diode's HSV values and converts its  left_diode_changed.rgb565 represantion.
 */
void change_left_diode_hsv(int hue_change, double saturation_change, double value_change);

/*
 * Zmeni HSV hodnoty barvy prave diody a prepocita jeji right_diode_changed.rgb565 reprezentaci.
 * Changes right diode's HSV values and converts its  right_diode_changed.rgb565 represantion.
 */
void change_right_diode_hsv(int hue_change, double saturation_change, double value_change);

#endif // _DIODES_H_
