#ifndef _MENU_NETWORK_H_
#define _MENU_NETWORK_H_

#include "global_settings.h"
#include "knobs.h"
#include "rgb565_colors.h"
#include "str_utils.h"
#include "network.h"

/*
 * Menu sitove komunikace.
 * Menu of network communication.
 */
void menu_network();

/*
 * Zobrazi menu sitove komunikace.
 * Shows menu of network communication.
 */
void show_menu_network(Found_listeners found_listeners, int shown_address);

/*
 * Zobrazi nalezenou IP adresu.
 * Shows found IP.
 */
void print_str_ip(unsigned int ui_ip);

#endif // _MENU_NETWORK_H_
