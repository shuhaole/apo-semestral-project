#ifndef _RGB565_COLORS_H_
#define _RGB565_COLORS_H_

/* -------------------------------------- DEFINED MACROS ------------------------------------------ */

#define WHITE 0xfffff
#define BLACK 0x0
#define RED 0xff000
#define GREEN 0x07E0
#define BLUE 0x001F

#endif // _RGB565_COLORS_H_
