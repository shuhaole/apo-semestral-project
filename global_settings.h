#ifndef _GLOBAL_SETTINGS_H_
#define _GLOBAL_SETTINGS_H_

#include <stdbool.h>

/* -------------------------------------- GLOBAL VARIABLES ------------------------------------------ */

// language: true = english, false = czech
bool english;
// knobs' sensibility for colors' HSV values changing
int knobs_sensibility;
// texts' size
int font_size;

/* -------------------------------------- FUNCTION DECLARATAION ------------------------------------------ */

/*
 * Nastavi globalni nastavitelne hodnoty do pocatecniho stavu.
 * Sets global adjustable variables to original state.
 */
void init_global_settings();

#endif // _GLOBAL_SETTINGS_H_
