#include "knobs.h"

void init_knobs() {
    knobs = map_phys_address(SPILED_REG_BASE_PHYS + SPILED_REG_KNOBS_8BIT_o, 4, false);
    knob_blue = knobs;
    knob_green = &knob_blue[1];
    knob_red = &knob_blue[2];
    knob_click = &knob_blue[3];
}
