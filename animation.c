#include "animation.h"

void start_screen() {
    set_background_color(BLACK);
    // static text
    char *txt = "Luky & Rolda:\0";
    Point point = find_string_start(txt, 1, 30, 0, 90, LCD_WIDTH);
    str2frame(txt, point.y, point.x, WHITE, 1);
    // animation
    txt = "APO: AbsolutnePrvotridniOvladac\0";
    point = find_string_start(txt, 2, 0, 0, LCD_HEIGHT, LCD_WIDTH);
	double hue = 0.0;
	double saturation = 0.0;
	double value = 0.0;
    str2frame(txt, point.y, point.x, BLACK, 2);
	repaint_lcd();
    clock_t start;
    for (int i = 0; i < 20; ++i) {
        // delay between animation steps
        start = clock();
        while (clock() - start < CLOCKS_PER_SEC / 50) {
        	// waits
        }
        str2frame(txt, point.y, point.x, hsvtorgb565(hue, saturation, value + 0.05 * i), 2);
		repaint_lcd();
    }
    // showing the result frame for a while
    start = clock();
    while (clock() - start < CLOCKS_PER_SEC * 3) {
		// waits
    }
}

void quit_screen(bool english) {
    set_background_color(BLACK);
    char *txt = "Nashledanou\0";
    if (english) {
        txt = "Good bye\0";
    }
    Point point = find_string_start(txt, 3, 0, 0, LCD_HEIGHT, LCD_WIDTH);
	double hue = 0;
	double saturation = 0.0;
	double value = 1.0;
    str2frame(txt, point.y, point.x, WHITE, 3);
	repaint_lcd();
    clock_t start;
    for (int i = 19; i > 0; --i) {
		// delay between animation steps
        start = clock();
        while (clock() - start < CLOCKS_PER_SEC / 20) {
			// waits
        }
        str2frame(txt, point.y, point.x, hsvtorgb565(hue, saturation, value + 0.05 * i), 3);
		repaint_lcd();
    }
    set_background_color(BLACK);
	repaint_lcd();
}
