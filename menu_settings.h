#ifndef _MENU_SETTINGS_H_
#define _MENU_SETTINGS_H_

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "global_settings.h"
#include "knobs.h"
#include "rgb565_colors.h"
#include "str_utils.h"
#include "menu_network.h"

/* -------------------------------------- GLOBAL VARIABLES ------------------------------------------ */

/*
 * Menu nastaveni jazyka, velikosti textu, citlivosti knobu a moznosti sitoveho menu.
 * Menu with settings of language, font size, knobs sensibility and network menu possibility.
 */
void menu_settings();

/*
 * Zobrazi menu nastaveni.
 * Shows settings menu.
 */
void show_menu_settings(int shown_row, int selected);

#endif // _MENU_SETTINGS_H_
