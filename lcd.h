#ifndef _LCD_H_
#define _LCD_H_

#include <stdint.h>
#include <stdbool.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

/* -------------------------------------- DEFINED MACROS ------------------------------------------ */

#define LCD_WIDTH 480
#define LCD_HEIGHT 320

#define LCD_WIDTH_HALF 240
#define LCD_HEIGHT_HALF 160

#define LCD_WIDTH_THIRD 160
#define LCD_WIDTH_TWO_THIRDS 320
#define LCD_HEIGHT_THIRD 107
#define LCD_HEIGHT_TWO_THIRDS 214

#define LCD_WIDTH_QUARTER 120
#define LCD_WIDTH_TWO_QUARTERS 240
#define LCD_WIDTH_THREE_QUARTERS 360
#define LCD_HEIGHT_QUARTER 80
#define LCD_HEIGHT_TWO_QUARTERS 160
#define LCD_HEIGHT_THREE_QUARTERS 240

#define LCD_HEIGHT_FIFTH 64
#define LCD_HEIGHT_TWO_FIFTHS 128
#define LCD_HEIGHT_THREE_FIFTHS 192
#define LCD_HEIGHT_FOUR_FIFTHS 256

/* -------------------------------------- GLOBAL VARIABLES ------------------------------------------ */

// LCD diplay's hardware address
uint32_t *lcd;
// matrix of pixels to be shown in display
uint16_t pixels[LCD_HEIGHT][LCD_WIDTH];

/* -------------------------------------- FUNCTION DECLARATAION ------------------------------------------ */

/*
 * Nacte HW adresu lcd displeje.
 * Assign hardware address to lcd's variable.
 */
void init_lcd();

/*
 * Prepise cely displej momentalnimi hodnotami pixelu v poli pixels.
 * Repaints the whole display with current values of pixels from pixels' matrix.
 */
void repaint_lcd();

/*
 * Nastavi vsechny pixely na danou barvu.
 * Sets all pixels in the matrix to the given color.
 */
void set_background_color(uint16_t color);

/*
 * Vybarvi dany obdelnik zadany levym hornim rohem [x_start, y_start] a pravym dolnim rohem [x_stop, y_stop] danou barvou color.
 * Sets the given rectangle's (given by upper left corne [x_start, y_start] and lower right corner [x_stop, y_stop]) color to the given color.
 */
void set_rect_color(uint16_t color, int y_start, int x_start, int y_stop, int x_stop);

#endif //_LCD_H_
