#ifndef _NETWORK_H_
#define _NETWORK_H_

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h>
#include <errno.h>
#include <netdb.h> 
#include "diodes.h"

/* -------------------------------------- DEFINED MACROS ------------------------------------------ */

#define BROADCAST_IP "255.255.255.255"
#define SLAVE_PORT 1500
#define MASTER_PORT 2000

#define COMMAND_LENGTH 16
#define BROADCAST_COMMAND "i'm your master\0"
#define SLAVE_COMMAND "i am your slave\0"
#define STRUCT_COMMAND 69360420

/* -------------------------------------- DEFINED STRUCTURES ------------------------------------------ */

/*
 * Slave boxes found IPs.
 */
typedef struct {
    int count;
    int allocated_size;
    struct sockaddr_in *listeners_addr;
} Found_listeners;

/*
 * Arguments for the master_listen function: slave boxes found IPs and display repaint command.
 */
typedef struct {
	Found_listeners *found_listeners;
	bool *repaint;
} Master_listen_args;

/*
 * Data structure with diodes' set up for network sending to a slave box.
 */
typedef struct {
	int command;
    Color left_diode;           
    Color left_diode_changed;   
    Color right_diode;         
    Color right_diode_changed;
    Diodes_bool sliding;
    Flashing flashing_left;
    Flashing flashing_right;
    Flashing flashing_both;
} Send_data;

/* -------------------------------------- GLOBAL VARIABLES ------------------------------------------ */

uint32_t my_ip;
pthread_t thread_id_slave_listen; 

/* -------------------------------------- FUNCTION DECLARATAION ------------------------------------------ */

/*
 * Ulozi mistni IP adresu a spusti slave poslouchaci vlakno.
 * Saves local IP and starts slave listening thread.
 */
void init_network();

/*
 * Ukonci slave poslouchaci vlakno.
 * Kills slave listening thread.
 */
void kill_network();

/*
 * Z mistniho hostname ziska mistni IP adresu.
 * Gets local IP address from the hostname.
 */
uint32_t get_my_ip();

/*
 * Posloucha na SLAVE_PORT a ceka na prichozi BROADCAST_COMMAND nebo datove ovladaci struktury Send_data.
 * Listens on a SLAVE_PORT and waits for an incoming BROADCAST_COMMAND or a data structure Send_data.
 */
void *slave_listen();

/*
 * Posloucha na MASTER_PORT a ceka na prichozi SLAVE_COMMAND. Uklada prijate slave IP.
 * Listens on a MASTER_PORT and waits for an incoming SLAVE_COMMAND. Saves received slave's IP.
 */
void *master_listen(void *args);

/*
 * Odesle broadcast dotaz na IP poslouchajicich desek.
 * Asks for IP of listening boxes by broadcast.
 */
void send_broadcast();

/*
 * Odesle momentalni nastaveni sviceni predane adrese.
 * Sends current local diodes' set up to given address.
 */
void send_control(struct sockaddr_in servaddr);

#endif // _NETWORK_H_
