#ifndef _STR_UTILS_H_
#define _STR_UTILS_H_

#include "font_types.h"
#include "lcd.h"

/* -------------------------------------- DEFINED MACROS ------------------------------------------ */

#define MASK_ROWS 16

/* -------------------------------------- DEFINED STRUCTURES ------------------------------------------ */

/*
 * Basic 2D description of the position by x and y coordinates.
 */
typedef struct {
    int x;
    int y;
} Point;

/* -------------------------------------- FUNCTION DECLARATAION ------------------------------------------ */

/*
 * Ulozi dany char c do matice pixelu. Horni levy roh bude [xcolumn, yrow]. Char bude mit forecolor barvu.
 * Return: Sirka ulozeneho charu v pixelech.
 * Saves given character c into the pixels' matrix. Upper left corner will be [xcolumn, yrow]. Char will have a forecolor color.
 * Return: Width of the saved char in pixels.
 */
int char2frame(char c, int yrow, int xcolumn, uint16_t forecolor, int size);
  
/*
 * Ulozi dany string str do matice pixelu. Horni levy roh prvni charu stringu bude [xcolumn, yrow]. String bude mit forecolor barvu.
 * Return: Sirka ulozeneho stringu v pixelech.
 * Saves given string str into the pixels' matrix. Upper left corner of the first character in the string will be [xcolumn, yrow]. String will have a forecolor color.
 * Return: Width of the saved string in pixels.
 */
int str2frame(char* str, int yrow, int xcolumn, uint16_t forecolor, int size);
  
/*
 * Vypocita levy horni roh stringu str tak, aby byl vycentrovany v obdelniku s levym hornim rohem [x_section_start, y_section_start], sirkou section_width a vyskou section_height.
 * Return: Souradnice leveho horniho rohu ve strukture Point.
 * Computes upper left corner of the string str so that the string is centered in a rectangle with upper left corner [x_section_start, y_section_start], width of section_width and height of section_height.
 * Return: Coordinates of the upper left corner in a Point structure.
 */
Point find_string_start(char *str, int str_font_size, int y_section_start, int x_section_start, int section_height, int section_width);

#endif // _STR_UTILS_H_
